# Project Title

CVE API Exercise

## Description

The Visual Studio 2019 source code for the Windows Console version of the CVE API Exercise.

## Getting Started

https://marketplace.visualstudio.com/items?itemName=MistyK.VisualStudioBitbucketExtension

 -or-
 
Clone like normal. Open the .sln file in Visual Studio.

### Dependencies

* Windows OS
* Visual Studio


### Executing program

To run in VS, simply hit F5, or navigate to Debug-->Start Debugging on the VS toolbar.

The console version of this exercise uses (horrifically and ashamedly) hardcoded values to
create an API call and save the output to a file in the same directory.

The call will always be for CVE-2020-0688, and the file will always be named api_output.json.


## Authors

Joshua Hurst


## Acknowledgments

* https://sites.google.com/site/wcfpandu/web-api/calling-a-web-api-from-c-and-calling-a-web-api-from-view
* https://youtu.be/aWePkE2ReGw
* https://docs.microsoft.com/en-us/aspnet/web-api/overview/advanced/calling-a-web-api-from-a-net-client
* https://youtu.be/b_8EfZkyMZg

### Additional Notes

If looking for just an executable, check out cve_search_app_console_published instead. 