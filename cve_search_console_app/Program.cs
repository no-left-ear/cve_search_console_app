﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace cve_search_console_app
{
    class ApiClient
    {
        // The output file
        static string outputFilename = Directory.GetCurrentDirectory() + "\\api_output.json";
        // The specifically requested CVE ID
        static string requestedData = "CVE-2020-0688";
        // The base API URL
        static string apiURL = "https://cve.circl.lu/api/";

        static void Main()
        {
            retrieveData().Wait();
        }

        // Takes a string of HTTP response data
        //   and writes it to a file in the same directory as the executable
        static async Task writeToFile(string apiRespData)
        {
            using (StreamWriter apiOutputFile = new StreamWriter(outputFilename))
            {
                await apiOutputFile.WriteAsync(apiRespData);
            }
            Console.WriteLine($"Creating file at {outputFilename}");
        }

        // Makes an HTTP API call to the specified endpoint
        // Calls the Task writeToFile and passes in the data
        static async Task retrieveData()
        {
            using (var apiClient = new HttpClient())
            {
                apiClient.BaseAddress = new Uri(apiURL);
                apiClient.DefaultRequestHeaders.Accept.Clear();
                apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage resp = await apiClient.GetAsync("cve/" + requestedData);
                if (resp.IsSuccessStatusCode)
                {
                    var respData = resp.Content.ReadAsStringAsync().Result;
                    await writeToFile(respData);
                }
            }
        }
    }
}